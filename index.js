/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userDetails(){
		let fullName = prompt("Enter your full name");
		let age = prompt("Enter your age:");
		let location = prompt("Enter your location");

		console.log("Hi " + fullName);
		console.log(fullName + "'s age is " + age);
		console.log(fullName + "'s location at " + location);
	};

userDetails();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function top5FavBands(){
		let favBandNames = ['The Script', 'Upper Room', 'Jema Galanza', 'Eraserheads', 'Planetshakers'];

		console.log(favBandNames);
	};

	top5FavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	*/
	
	//third function here:
	function myTop5Movies(){
		let movieName1 = "The Internship";
		let movieName2 = "Grown Ups";
		let movieName3 = "3 idiots";
		let movieName4 = "The Accountant";
		let movieName5 = "The Terminal";

		let movie1tomatometer = 35;
		let movie2tomatometer = 11;
		let movie3tomatometer = 100;
		let movie4tomatometer = 52;
		let movie5tomatometer = 61;

		console.log("1. "+ movieName1);
		console.log("Tomatometer for " + movieName1 + ": " + movie1tomatometer + "%");
		console.log("2. "+ movieName2);
		console.log("Tomatometer for " + movieName2 + ": " + movie2tomatometer + "%");
		console.log("3. " + movieName3);
		console.log("Tomatometer for " + movieName3 + ": " + movie3tomatometer+ "%");
		console.log("4. " +movieName4);
		console.log("Tomatometer for " + movieName4 + ": " + movie4tomatometer + "%");
		console.log("5. " +movieName5);
		console.log("Tomatometer for " + movieName5 + ": " + movie5tomatometer + "%");
	};

	myTop5Movies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function (){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
